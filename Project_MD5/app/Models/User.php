<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    protected $table ="users";
    protected $fillable = ['id','name','role','email','phone','gender','address','birthday','password'];
    public $timestamps = false;
}
