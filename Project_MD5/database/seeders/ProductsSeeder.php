<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = new Product();
        $products->name         = "Đồi Thỏ";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'bank-4.0.jpg';
        $products->quantity     = '10';
        $products->category_id  = 1 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Nguyễn Nhi';
        $products->save();    

        $products = new Product();
        $products->name         = " Đi tìm lẽ sống";
        $products->price        = '300.000 VND';
        $products->description  = 'Hay ,Ý nghĩa';
        $products->image        = 'combo-chuyen-nghe-chuyen-doi.jpg';
        $products->quantity     = '10';
        $products->category_id  = 2 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Herman Melville';
        $products->save();    

        $products = new Product();
        $products->name         = "Vũ trụ";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-buoi-sang-dieu-ky.jpg';
        $products->quantity     = '10';
        $products->category_id  = 3 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Ernest Hemingway';
        $products->save();    

        $products = new Product();
        $products->name         = "A Survivor's Tale";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-buoi-sang-dieu-ky.jpg';
        $products->quantity     = '10';
        $products->category_id  = 3 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Haruki Murakami';
        $products->save();    

        $products = new Product();
        $products->name         = "Chuông nguyện hồn ai";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-buoi-sang-dieu-ky.jpg';
        $products->quantity     = '10';
        $products->category_id  = 2 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Antoine de Saint-Exupéry';
        $products->save();    

        $products = new Product();
        $products->name         = "Hoàng tử bé";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-buoi-sang-dieu-ky.jpg';
        $products->quantity     = '10';
        $products->category_id  = 4 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Cormac McCarthy';
        $products->save();    

        $products = new Product();
        $products->name         = "Trăm năm cô đơn ";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-an-xanh-song-lanh.jpg';
        $products->quantity     = '10';
        $products->category_id  = 10 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Gabriel Garcia Marquez';
        $products->save();    

        $products = new Product();
        $products->name         = "Phía Đông vườn Địa đàng";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-an-xanh-song-lanh.jpg';
        $products->quantity     = '10';
        $products->category_id  = 8 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'John Steinbeck';
        $products->save();    

        $products = new Product();
        $products->name         = "Anh em nhà Karamazov";
        $products->price        = '300.000 VND';
        $products->description  = 'Ý nghĩa nhân văn, Mô tả nội dung đời thực';
        $products->image        = 'combo-an-xanh-song-lanh.jpg';
        $products->quantity     = '10';
        $products->category_id  = 7 ;
        $products->status       = 'Còn Hàng';
        $products->supplier_id  = 'Nhà cung cấp Phát Đạt';
        $products->author_id    = 'Albert Camus';
        $products->save();    
    }
}
